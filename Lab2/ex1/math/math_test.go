package math

import "testing"

func TestFindMin(t *testing.T) {
	x := FindMin(1, 2, -3)
	res := -3
	if x != res {
		t.Errorf("Тест не пройдений! Результат %f, а повинен бути %f", x, res)
	}
}

func TestFindAvg(t *testing.T) {
	x := FindAvg(1, 2, 3)
	res := 2
	if x != res {
		t.Errorf("Тест не пройдений! Результат %f, а повинен бути %f", x, res)
	}
}

func TestSolve(t *testing.T) {
	x := Solve(10, -23)
	res := 2.300000
	if x != res {
		t.Errorf("Тест не пройдений! Результат %f, а повинен бути %f", x, res)
	}
}
