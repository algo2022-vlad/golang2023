package math

import "strconv"

func FloatToString(n float64) string {
	return strconv.FormatFloat(n, 'f', 6, 64)
}

func FindMin(x1 float64, x2 float64, x3 float64) float64 {
	if x2 < x1 {
		x1 = x2
	}
	if x3 < x1 {
		x1 = x3
	}
	return x1
}

func FindAvg(x1 float64, x2 float64, x3 float64) float64 {
	if ((x1 + x2 + x3) / 3) != 0 {
		return (x1 + x2 + x3) / 3
	}
	return 0
}

func Solve(a float64, b float64) string {

	//ax+b=0
	if a == 0 && b != 0 {
		return "Немає рішень."
	}
	if a == 0 && b == 0 {
		return "Х Є R"
	}
	return FloatToString((-1 * b) / a)

}
