// main
package main

import (
	mymath "ex1/math"
	"fmt"
)

var a, b, d, e, f float64
var sum string

func main() {
	d = 81118.0
	e = 167651.1
	f = -234567.99
	fmt.Print("d = ", d)
	fmt.Print("; e = ", e)
	fmt.Print("; f = ", f)
	fmt.Print("\nMin value = ", mymath.FindMin(d, e, f))
	fmt.Print("; Average = ", mymath.FloatToString(mymath.FindAvg(d, e, f)))
	a = 10
	b = -23
	fmt.Print("\na = ", a)
	fmt.Print("; b = ", b)
	sum := mymath.Solve(a, b)
	fmt.Println("\nResult of first-order equation:", sum)
}
