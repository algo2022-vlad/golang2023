package main

import "fmt"

func main() {
	//Инициализация переменных
	var userinit8 uint8 = 1
	var userinit16 uint16 = 2
	var userinit64 int64 = -3
	var userautoinit = -4 //Такой вариант инициализации также возможен

	fmt.Println("Values: ", userinit8, userinit16, userinit64, userautoinit, "\n")

	//Краткая запись объявления переменной
	//только для новых переменных
	intVar := 10

	fmt.Printf("Value = %d Type = %T\n", intVar, intVar)

	//Задание.
	//1. Вывести типы всех переменных
	//2. Присвоить переменной intVar переменные userinit16 и userautoinit. Результат вывести.
	fmt.Println("Type of userinit8: %T\n", userinit8)
	fmt.Println("Type of userinit16: %T\n", userinit16)
	fmt.Println("Type of userinit64: %T\n", userinit64)
	fmt.Println("Type of userautoinit: %T\n", userautoinit)

	//cannot use userinit16 (variable of type uint16) as int value in assignment
	//intVar = userinit16
	//fmt.Println("intVar = userinit16:", userinit16)

	intVar2 := userinit16 // new var
	fmt.Println("intVar2 = userinit16:", intVar2)

	intVar = userautoinit
	fmt.Println("intVar = userautoinit:", intVar)
}
