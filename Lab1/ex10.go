package main

import "fmt"

func main() {
	var chartype int8 = 'R'

	fmt.Printf("Code '%c' - %d\n\n", chartype, chartype)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	//2. Пояснить назначение типа "rune"

	char := 'Ї'
	fmt.Printf("Symbol '%c'\n", char)
	fmt.Printf("Code '%c' - %d\n", char, char)

	fmt.Printf("rune дозволяє працювати з символами різних розмірів та кодувань.\n")
	fmt.Printf("Також представляє коди символів Unicode та використовується для роботи")
	fmt.Printf("з символами та текстом в форматі Unicode\n")
}
