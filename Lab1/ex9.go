package main

import "fmt"

func main() {
	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true

	//Задание.
	//1. Пояснить результаты операций

	fmt.Println("first  = ", first)       // по замовчуванню first буде 0, тобто false
	fmt.Println("second = ", second)      // те саме, що first
	fmt.Println("third  = ", third)       // ініціалізували third як true
	fmt.Println("fourth = ", fourth)      // !third -> !true = false
	fmt.Println("fifth  = ", fifth, "\n") // ініціалізували fifth як true

	fmt.Println("!true  = ", !true)        // !true = false
	fmt.Println("!false = ", !false, "\n") // !false = true

	// логічне І
	fmt.Println("true && true   = ", true && true)         // true - якщо обидва значення дорівнюють true, результат буде true
	fmt.Println("true && false  = ", true && false)        // false - якщо хоч одне значення false, результат буде false
	fmt.Println("false && false = ", false && false, "\n") // false - якщо хоч одне значення false, результат буде false

	// логічне АБО
	fmt.Println("true || true   = ", true || true)         // true - якщо хоч одне значення true, то результат буде true
	fmt.Println("true || false  = ", true || false)        // true - якщо хоч одне значення true, то результат буде true
	fmt.Println("false || false = ", false || false, "\n") // false - якщо обидва значення false, то результат буде false

	fmt.Println("2 < 3  = ", 2 < 3)        // 2 менше за -> true
	fmt.Println("2 > 3  = ", 2 > 3)        // 2 не більше за 3 -> false
	fmt.Println("3 < 3  = ", 3 < 3)        // 3 не менше за 3 -> false
	fmt.Println("3 <= 3 = ", 3 <= 3)       // 3 менше або рівне 3 -> true
	fmt.Println("3 > 3  = ", 3 > 3)        // 3 не більше 3 -> false
	fmt.Println("3 >= 3 = ", 3 >= 3)       // 3 більше або рівне 3 -> true
	fmt.Println("2 == 3 = ", 2 == 3)       // 2 не дорівнює 3 -> false
	fmt.Println("3 == 3 = ", 3 == 3)       // 3 дорівнює 3 -> true
	fmt.Println("2 != 3 = ", 2 != 3)       // 2 не дорівнює 3 -> !false = true
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // 3 дорівнює 3 -> !true = false
}
