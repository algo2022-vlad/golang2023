package main

import "fmt"

func main() {
	variable8 := int8(127)
	variable16 := int16(16383)

	fmt.Println("Приведение типов\n")

	fmt.Printf("variable8         = %-5d = %.16b\n", variable8, variable8)
	fmt.Printf("variable16        = %-5d = %.16b\n", variable16, variable16)
	fmt.Printf("uint16(variable8) = %-5d = %.16b\n", uint16(variable8), uint16(variable8))
	fmt.Printf("uint8(variable16) = %-5d = %.16b\n", uint8(variable16), uint8(variable16))

	//Задание.
	//1. Создайте 2 переменные  разных типов. Выпоните арифметические операции. Результат вывести

	var16 := int16(1234)
	var64 := int64(54321)

	plus := var64 + int64(var16)
	fmt.Println("int64+int16 =", plus)

	minus := var64 - int64(var16)
	fmt.Println("int64-int16 =", minus)

	mult := int16(var64) * var16
	fmt.Println("int64*int16 =", mult)

	divide := int16(var64) / var16
	fmt.Println("int64/int16 =", divide)
}
