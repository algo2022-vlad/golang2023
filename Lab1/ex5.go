package main

import "fmt"

func main() {
	fmt.Println("Синонимы целых типов\n")

	fmt.Println("byte    - int8")
	fmt.Println("rune    - int32")
	fmt.Println("int     - int32, или int64, в зависимости от ОС")
	fmt.Println("uint    - uint32, или uint64, в зависимости от ОС")

	//Задание.
	//1. Определить разрядность ОС

	var intVar1 int = 2147483647
	intVar1 += 1
	var intVar2 uint = 4294967295
	intVar2 += 1
	var intVarb int = 0b11111111111111111111111111111111
	intVarb += 1

	fmt.Printf("Value = %d Type = %T\n", intVar1, intVar1)
	fmt.Printf("Value = %d Type = %T\n", intVar2, intVar2)
	fmt.Printf("Value (bin) = %b Value (dec) = %d Type = %T\n", intVarb, intVarb, intVarb)
}
