package main

import (
	"fmt"
	"math"
	"time"
)

/*варіант 4*/
func main() {
	x := uint32(time.Now().Unix())

	var m = math.MaxUint32
	const c = 1
	const a = 22695477

	var xn, n uint32
	var arr [40000]float64

	for i := 0; i < 40000; i++ {

		xn = (a*x + c) % uint32(m)
		x = xn
		n = xn % 250
		realNumber := float64(n) / 250.0

		fmt.Printf("%6.2f", realNumber)
		arr[i] = realNumber
	}

}
