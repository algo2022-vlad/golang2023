package main

import (
	"fmt"
	"math"
	"time"
)

/*варіант 4*/
func main() {
	x := uint32(time.Now().Unix())

	var m = math.MaxUint32
	const c = 1
	const a = 22695477

	var xn, n uint32
	var arr [40000]uint32

	for i := 0; i < 40000; i++ {

		xn = (a*x + c) % uint32(m)
		x = xn
		n = xn % 250
		fmt.Printf("%5d", n)
		arr[i] = n
	}

	var L int
	var P, matRand, dispersia float64

	for i := 0; i < 250; i++ {
		L = 0
		for j := 0; j < 40000; j++ {
			if arr[j] == uint32(i) {
				L++
			}
		}
		P = float64(L) / 40000.0
		matRand += float64(i) * P
		fmt.Printf("\n--------------------------------------------------------------------\n")
		fmt.Printf("Число: %3d\nСтатистична імовірність появи випадкових величин: %f\n",
			i, P)
	}

	fmt.Printf("\nМатематичне сподівання випадкових величин: %f\n", matRand)

	for i := 0; i < 250; i++ {
		L = 0
		for j := 0; j < 40000; j++ {
			if arr[j] == uint32(i) {
				L++
			}
		}
		P = float64(L) / 40000.0
		dispersia += math.Pow(float64(i)-matRand, 2) * P
	}

	fmt.Printf("\n------------------------------------------\n")
	fmt.Printf("Дисперсія випадкових величин: %f\n", dispersia)
	avgDeviation := math.Sqrt(dispersia)
	fmt.Printf("Середньоквадратичне відхилення випадкових величин: %f", avgDeviation)

}
