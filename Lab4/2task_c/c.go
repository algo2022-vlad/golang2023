package main

/*
#include <stdio.h>
#include <stdlib.h>


double calculateCost(double days, double multiplier, int guide, int luxury) {
    double result = days * multiplier;
    if (guide) {
        result += (days * 50.0);
    }
    if (luxury) {
        result += (result * 0.2);
    }
    return result;
}
*/
import "C"

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func main() {
	err := ui.Main(func() {

		mainWin := ui.NewWindow("Calculator", 300, 200, true)
		mainWin.SetMargined(true)

		daysLabel := ui.NewLabel("Кількість днів:")
		daysEntry := ui.NewEntry()
		countryList := ui.NewCombobox()
		countryList.Append("Болгарія")
		countryList.Append("Німеччина")
		countryList.Append("Польща")
		seasonList := ui.NewCombobox()
		seasonList.Append("Літо")
		seasonList.Append("Зима")
		guideCheckbox := ui.NewCheckbox("Гід")
		luxuryCheckbox := ui.NewCheckbox("Люкс")
		calculateButton := ui.NewButton("Розрахувати")

		resultLabel := ui.NewLabel("")

		vbox := ui.NewVerticalBox()
		vbox.Append(daysLabel, false)
		vbox.Append(daysEntry, false)
		vbox.Append(guideCheckbox, false)
		vbox.Append(luxuryCheckbox, false)
		vbox.Append(countryList, false)
		vbox.Append(seasonList, false)
		vbox.Append(calculateButton, false)
		vbox.Append(resultLabel, false)

		mainWin.SetChild(vbox)

		calculateButton.OnClicked(func(*ui.Button) {
			selectedCountryIndex := countryList.Selected()
			selectedSeasonIndex := seasonList.Selected()
			multiplier := 0.0
			if selectedCountryIndex != -1 && selectedSeasonIndex != -1 {
				if selectedSeasonIndex == 0 {
					switch selectedCountryIndex {
					case 0:
						multiplier = 100.0
						break
					case 1:
						multiplier = 160.0
						break
					case 2:
						multiplier = 120.0
						break
					}
				} else {
					switch selectedCountryIndex {
					case 0:
						multiplier = 150.0
						break
					case 1:
						multiplier = 200.0
						break
					case 2:
						multiplier = 180.0
						break
					}
				}
				daysValue := daysEntry.Text()
				days, errDays := strconv.ParseFloat(daysValue, 64)

				if errDays != nil {
					return
				}

				result := C.calculateCost(C.double(days), C.double(multiplier), C.int(convert(guideCheckbox.Checked())), C.int(convert(luxuryCheckbox.Checked())))

				resultLabel.SetText(fmt.Sprintf("%f", result))

			} else {
				resultLabel.SetText("")
			}
		})

		mainWin.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		mainWin.Show()
	})

	if err != nil {
		panic(err)
	}
	ui.Quit()
}

func convert(b bool) int {
	if b {
		return 1
	}
	return 0
}
