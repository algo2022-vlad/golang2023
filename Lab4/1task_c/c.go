package main

/*
#include <stdio.h>
#include <stdlib.h>

double calculateCost(double width, double height, double multiplier, int subwindow) {
    double result = width * height * multiplier;
    if (subwindow) {
        result += 350.0;
    }
    return result;
}
*/
import "C"

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func main() {
	err := ui.Main(func() {

		mainWin := ui.NewWindow("Calculator", 300, 200, true)
		mainWin.SetMargined(true)

		widthLabel := ui.NewLabel("Ширина:")
		widthEntry := ui.NewEntry()
		heightLabel := ui.NewLabel("Висота:")
		heightEntry := ui.NewEntry()
		materialList := ui.NewCombobox()
		materialList.Append("Дерев'яний")
		materialList.Append("Металевий")
		materialList.Append("Металопластиковий")
		cameraList := ui.NewCombobox()
		cameraList.Append("Однокамерний")
		cameraList.Append("Двокамерний")
		subwindowCheckbox := ui.NewCheckbox("Підвіконня")
		calculateButton := ui.NewButton("Розрахувати")

		resultLabel := ui.NewLabel("")

		vbox := ui.NewVerticalBox()
		vbox.Append(widthLabel, false)
		vbox.Append(widthEntry, false)
		vbox.Append(heightLabel, false)
		vbox.Append(heightEntry, false)
		vbox.Append(subwindowCheckbox, false)
		vbox.Append(materialList, false)
		vbox.Append(cameraList, false)
		vbox.Append(calculateButton, false)
		vbox.Append(resultLabel, false)

		mainWin.SetChild(vbox)

		calculateButton.OnClicked(func(*ui.Button) {
			selectedMaterialIndex := materialList.Selected()
			selectedCameraIndex := cameraList.Selected()
			multiplier := 0.0
			if selectedMaterialIndex != -1 && selectedCameraIndex != -1 {
				if selectedCameraIndex == 0 {
					switch selectedMaterialIndex {
					case 0:
						multiplier = 2.5
						break
					case 1:
						multiplier = 0.5
						break
					case 2:
						multiplier = 1.5
						break
					}
				} else {
					switch selectedMaterialIndex {
					case 0:
						multiplier = 3.0
						break
					case 1:
						multiplier = 1.0
						break
					case 2:
						multiplier = 2.0
						break
					}
				}
				widthValue := widthEntry.Text()
				heightValue := heightEntry.Text()
				width, errWidth := strconv.ParseFloat(widthValue, 64)
				height, errHeight := strconv.ParseFloat(heightValue, 64)
				if errWidth != nil || errHeight != nil {
					return
				}

				result := C.calculateCost(C.double(width), C.double(height), C.double(multiplier), C.int(convert(subwindowCheckbox.Checked())))

				resultLabel.SetText(fmt.Sprintf("%f", result))

			} else {
				resultLabel.SetText("")
			}
		})

		mainWin.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		mainWin.Show()
	})

	if err != nil {
		panic(err)
	}
	ui.Quit()
}

func convert(b bool) int {
	if b {
		return 1
	}
	return 0
}
